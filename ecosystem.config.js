module.exports = {
    apps: [
        {
            name: 'run-artisan-server',
            interpreter: 'php',
            script: 'artisan',
            args: 'serve --host 172.31.6.203',
            instances: 1,
            autorestart: true,
            watch: false,
            max_memory_restart: '1G'
        },
        {
            name: 'run-webspockets-server',
            interpreter: 'php',
            script: 'artisan',
            args: 'websockets:serve --host 172.31.6.203',
            instances: 1,
            autorestart: true,
            watch: false,
            max_memory_restart: '1G'
        }
    ]
};
