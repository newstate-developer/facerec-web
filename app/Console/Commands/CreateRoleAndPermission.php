<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateRoleAndPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:roleandpermissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $superadmin = Role::firstOrCreate(['name' => 'super-admin']);
        $manager = Role::firstOrCreate(['name' => 'manager']);
        $employee = Role::firstOrCreate(['name' => 'employee']);

        // Permission::findOrCreate(config('permissiontype.assignrequsition'));
        // Permission::findOrCreate(config('permissiontype.getrequsition'));
        // Permission::findOrCreate(config('permissiontype.checkrequsition'));

        // $supervisor->givePermissionTo(config('permissiontype.assignrequsition'));
        // $supervisor->givePermissionTo(config('permissiontype.getrequsition'));
        // $supervisor->givePermissionTo(config('permissiontype.checkrequsition'));

        // $selector->givePermissionTo(config('permissiontype.getrequsition'));

        // $checker->givePermissionTo(config('permissiontype.checkrequsition'));


        $this->info('Permissions created successfully.');
    }
}
