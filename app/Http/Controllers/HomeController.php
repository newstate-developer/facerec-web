<?php
namespace App\Http\Controllers;
use App\Events\MyEvent;
use App\Images;
use App\IncidentAssignment;
use App\Message;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function postSendMessage(Request $request)
    {
        $message = new Message();
        $message->from_user = Auth::user()->id;
        $message->image_id = $request->image_id;
        $message->content = $request->msg;
        $message->save();
        $message->dateTimeStr = date("Y-m-dTH:i", strtotime($message->created_at->toDateTimeString()));
        // $message->dateHumanReadable = $message->created_at->diffForHumans();
        // $message->fromUserName = $message->fromUser->name;
        // $message->from_user_id = Auth::user()->id;
        // $message->toUserName = $message->toUser->name;
        // $message->to_user_id = $request->to_user;
        return redirect()->back();
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }
    public function assignroles()
    {
        return view('assignroles');
    }
    public function timeline($imgid)
    {
        $incidenttimeline = IncidentAssignment::where('image_id',$imgid)
        ->with('User')->with('Image')->get();
        $messages = Message::where('image_id',$imgid)->get();

        $results = $messages->concat($incidenttimeline);

        $results = $results->sortBy('created_at')->values()->all();
        $results = collect($results)->reverse();
        return view('timeline',[
            "results" =>$results,
            'imgid'=>$imgid
        ]);
    }
    public function myassign()
    {
        $user = \Auth::user();
        if (!$user->hasRole('super-admin')) {
            $myassign= IncidentAssignment::where('user_id',$user->id)
            ->orderBy('created_at', 'DESC')
            ->groupBy('image_id')->get();
            $latest = new Collection();
            foreach ($myassign as $assign) {
                $latest->add(
                    // Find each group's latest row using Eloquent + standard modifiers.
                    IncidentAssignment::where([
                            'user_id' => $assign->user_id,
                            'image_id' => $assign->image_id,
                        ])
                        ->with('Image')
                        ->with('User')
                        ->latest()
                        ->first()
                );
            }
            $myassign = $latest->reverse();
        }else{
            $myassign= IncidentAssignment::orderBy('created_at', 'DESC')
            ->groupBy('image_id')->get();
            $latest = new Collection();
            foreach ($myassign as $assign) {
                $latest->add(
                    // Find each group's latest row using Eloquent + standard modifiers.
                    IncidentAssignment::where([
                            'image_id' => $assign->image_id,
                        ])
                        ->with('Image')
                        ->with('User')
                        ->latest()
                        ->first()
                );
            }
            $myassign = $latest->reverse();
        }

        return view('myassign',[
            'myassign'=>$myassign
        ]);
    }
    public function saveuserstatus(Request $request)
    {
        $check = IncidentAssignment::where('user_id',Auth::id())
        ->where('image_id',$request->image_id)
        ->where('still_assigned',1)
        ->first();
        if ($check){
        $incident = Images::find($request->image_id);
        if($request->action == "Active")
        {
            $incident->status = 1;
        }
        if($request->action == "Complete")
        {
            $incident->status = 2;
        }
        $incident->save();
    }
    return redirect()->back();
    }
    public function saveroles(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:3',
            'role'=>'required|min:2',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password =  Hash::make($request->input('password'));
        $user->save();
        $user->assignRole($request->role);
        return  response()->json($user);
    }
    public function edituser(Request $request)
    {

        $users = User::all()->except(Auth::id());


       return view('edituser',[
           'users'=>$users
       ]);


    }
    public function editrole(Request $request)
    {
       $request->validate(
           [
                'user_id' => 'required|integer',
               'role' => 'required|min:2',
           ]);
           $id = User::find($request->user_id);
           $id->roles()->detach();
           $id->assignRole($request->role);
           return redirect()->back();
    }


}
