<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Events\ReceivedNewImage;
use App\Images;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CaptureController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        return view('capture');
    }

    public function saveimage(Request $request)
    {
        $imgname =  saveFile($request);
        $images = new Images();
        $images->imagename = $imgname;
        $images->imagepath = 'faces/'.$imgname.".png";
        $images->capturefrom =$request->cam_name;
        $images->save();
        event(new MyEvent('hello world'));
    }

}
    function saveFile(Request $request) {
        $image = $request->image;
        $image = str_replace(' ', '+', $image);
        $imagename = "incident-".Carbon::now()
        ->timezone('Asia/Karachi')
        ->toDateTimeString();
        $imagename = str_replace(" ","",$imagename);
        $imagename = str_replace("-","",$imagename);
        $imagename = str_replace(":","",$imagename);
        $imagename = $imagename.'.png';
        $path = public_path().'/images/faces/' . $imagename;
        $file = base64_decode($image);
        file_put_contents($path, $file);
        return str_replace(".png","",$imagename);
    }
