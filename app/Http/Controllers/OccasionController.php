<?php

namespace App\Http\Controllers;

use App\Images;
use App\IncidentAssignment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class OccasionController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        return view('occasions');
    }

    public function GetIncidents(Request $request)
    {
        if ($request->filter) {
             $occasions = Images::whereDate('created_at','>',Carbon::now()->subDays($request->filter))
             ->latest()
             ->paginate(12);
             return $occasions;
         }
         $occasions = Images::latest()->paginate(12);
         return $occasions;
    }

    public function updateoccasion(Request $request)
    {
        $request->validate([
            'occasionid' => 'required|integer',
            'occasiondesc'  => 'nullable|min:3',
            'userid'=>'nullable'
        ]);
        if ($request->userid!=null){
            $lastassignment = IncidentAssignment::where('image_id',$request->occasionid)
            ->latest()->first();
            if($lastassignment){
                $incidentassigns = IncidentAssignment::create([
                    'image_id'=>$lastassignment->image_id,
                    'user_id'=> $lastassignment->user_id,
                    'still_assigned'=>0
                ]);
            }
            sleep( 1);
            $incidentassigns = IncidentAssignment::create([
                'image_id'=>$request->occasionid,
                'user_id'=> $request->userid,
                'still_assigned'=>1
            ]);
            $incidentassigns->save();

        }
        $occasion = Images::find($request->occasionid);
        $occasion->description = $request->occasiondesc;
        $occasion->save();
        return response()->json('',200);

    }
}
