<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentAssignment extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_id','user_id','still_assigned'
    ];

    public function User()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function Image()
    {
        return $this->belongsTo(Images::class,'image_id');
    }
}
