<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    public function Users()
    {
        return $this->belongsToMany(User::class, 'incident_assignments', 'image_id', 'user_id');
    }

    public function IncidentAssignment()
    {
        return $this->hasMany(IncidentAssignment::class,'image_id');
    }
    // public function displayimages()
    // {
    //     return $this->hasMany(IncidentAssignment::class,'content');
    // }
}
