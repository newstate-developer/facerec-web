<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Spatie\Permission\Models\Role;

Route::get('/', 'HomeController@index');
Route::get('/incident', 'OccasionController@index')->name('incident');
Route::get('/getincidents', 'OccasionController@GetIncidents');
Route::get('/capture', 'CaptureController@index');
Route::get('/assignroles', 'HomeController@assignroles');

Route::post('/assignroles', 'HomeController@saveroles');
Route::post('/saveimage','CaptureController@saveimage');
Route::get('/myassign', 'HomeController@myassign');
Route::post('/updateoccasion','OccasionController@updateoccasion');
Route::post('/assignroles', 'HomeController@saveuserstatus');
Route::get('/timeline/{imgid}', 'HomeController@timeline');
Route::get('/load-latest-messages', 'HomeController@getLoadLatestMessages');


    Route::get('/edituser', 'HomeController@edituser')->middleware('admin');
    Route::post('/editrole','HomeController@editrole');


Route::post('/send', 'HomeController@postSendMessage');

Route::get('/getusers',function ()
{
    $role = Role::all();
    $users=User::with('roles')->get();
    $nonmembers=$users->reject(function($users, $key){
        return$users->hasRole('super-admin');
    });
    $nonmembers=$nonmembers->reject(function($users, $key){
        return$users->hasRole('super-admin');
    });
    return $nonmembers->flatten();
});
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
// Route::get('/home', 'HomeController@index')->name('home');
