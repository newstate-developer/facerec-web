@extends('layout.baselayout')
@section('title',"Predict")
@section('pagecss')
<link rel="stylesheet" href="css/json-viewer.css">
@endsection
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
    <!-- page start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Real Time Prediction
                        {{-- <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span> --}}
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <div id="predictcamera">

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-12">
                                    <button id="predictme" style="margin-top: 20px;" type="button" class="btn btn-block btn-lg btn-primary btn-default">Single click prediction</button>
                                </div>
                                <div style="padding-top:20px" class="col-lg-12 form-inline">
                                    <h2>Real Time Prediction</h2>
                                    <div style="padding-left: 0px;" class="form-group col-lg-8">
                                        <label>Predict After</label>
                                        <input style="width: -webkit-fill-available;" value="3" type="number" class="form-control input-normal" id="timer" placeholder="Timer">
                                    </div>
                                    <div style="padding-top: 22px;"  class="form-group">
                                        <button id="free-predict" type="button" onclick="take_snapshot(event)" class="btn btn-info">Start Real time prediction</button>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="padding-top: 10px;" id="json"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
@endsection

@section('pagescripts')
<script src="js/webcam.min.js"></script>
<script src="js/json-viewer.js"></script>
<script src="js/notify.min.js"></script>
<script>
    // baguetteBox.run('.tz-gallery');
    var baseurl = "https://71484552.ngrok.io/api/";
    var jsonViewer = new JSONViewer();
    document.querySelector("#json").appendChild(jsonViewer.getContainer());
    var timeout;
    Webcam.set({
        width: 852,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100,
        cam_name: "cam_dev"
    });
    Webcam.attach('#predictcamera');
    // Webcam.attach('#capturecamera');
    $("#predictcamera").removeAttr('style');
    // $("#capturecamera").removeAttr('style');


    $('#predictme').click(function() {
        $(this).attr("disabled", true);
        $.notify("Sending Image to Server!",'info');
        var that = $(this);
        Webcam.snap(function(data_uri) {
            var start_time = new Date().getTime();
            console.log(data_uri)
            Webcam.upload(data_uri, baseurl+"predict", function(code, text) {
                var request_time = new Date().getTime() - start_time;
                console.log(text);
                if (code == 200) {
                    $.notify("Got Response from Server!",'info');
                    let resobj = JSON.parse(text);
                    resobj.timetaken = request_time + 'ms';
                    jsonViewer.showJSON(resobj);
                }else{
                    $.notify("Server Error!",'error');
                }
                $(that).removeAttr("disabled");
            });
        });
    });
    $('#train').click(function() {
        if ($('#name').val().length > 2) {
            $(this).attr("disabled", true);
            var that = $(this);
            Webcam.snap(function(data_uri) {
                Webcam.upload(data_uri,baseurl+ "train", function(code, text) {
                    console.log(text);
                    if (code == 200) {
                        jsonViewer.showJSON(text);
                    }
                    $(that).removeAttr("disabled");
                });
            });
        } else {
            alert("Please Enter a Valid Name");
        }
    });

    function take_snapshot(event) {
        console.log(event.target.innerText)
        if (event.target.innerText == "Start Real time prediction") {
            let interval;
            if (Number.isInteger($('#timer').val())) {
                interval = $('#timer').val() * 1000;
            } else{
                interval = 3 * 1000;
            }
            timeout = setInterval(function() {
                Webcam.snap(function(data_uri) {
                    $.notify("Sending Image to Server!",'info');
                    console.log(data_uri)
                    Webcam.upload(data_uri, baseurl+"predict", function(code, text) {
                        console.log(text);
                        if (code == 200) {
                            $.notify("Got Response from Server!",'info');
                            console.log(text)
                            jsonViewer.showJSON(JSON.parse(text));
                        }
                    });
                });
            }, interval);
            event.target.innerText = "Stop real time prediction";
        } else {
            clearTimeout(timeout);
            event.target.innerText = "Start Real time prediction";
        }

    }
</script>
@endsection
