@extends('layout.baselayout')

@section('title','My Assign')
@section('content')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
    <!-- page start-->
<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Basic Forms
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form method="POST" action="/assignroles" role="form">
                                <div class="form-group">
                                    @csrf
                                    <label for="exampleInputName">Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Enter name">
                                @error('name')
                                    <p style="color: red">{{$message}}</p>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                                    @error('email')
                                    <p style="color: red">{{$message}}</p>
                                @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" name="password"class="form-control" placeholder="Password">
                                    @error('password')
                                    <p style="color: red">{{$message}}</p>
                                @enderror
                                </div>
                                <div class="form-group">
                                <label for="exampleInputSelectRole">Select Role</label>
                                <select name="role" class="form-control">
       @if(Auth::user()->hasRole('super-admin'))                         
  <option value="super-admin">SuperAdmin</option>
  <option value="manager">Manager</option>
  <option value="employee">Employee</option>
  @endif
  @if(Auth::user()->hasRole('manager'))                         
  
  <option value="manager">Manager</option>
  <option value="employee">Employee</option>
  @endif
</select>

                                </div>
                                
                                <button type="submit" class="btn btn-info">Submit</button>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
            
                    </div>
                </section>

            </div>
        </div>

        </section>
</section>
        @stop

@section('pagescripts')

@stop


