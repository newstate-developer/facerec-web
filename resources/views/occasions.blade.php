@extends('layout.baselayout')
@section('title','Incident')
@section('pagecss')
<link href="{{asset('vendor/lightbox/css/lightbox.css')}}" rel="stylesheet" />
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

@endsection
@section('content')
        <!--main content start-->
        <section id="main-content">
            <section id="app" class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            All Occasions
                            {{-- <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span> --}}
                        </header>
                        <div id="loader-wrapper">
                            <div id="loader"></div>

                            <div class="loader-section section-left"></div>
                            <div class="loader-section section-right"></div>

                        </div>
                        <div class="panel-body" id="mainincident">

                            {{-- <ul id="filters" class="media-filter">
                                <li><a href="#" data-filter="*"> All</a></li>
                                <li><a href="#" data-filter=".images">Images</a></li>
                                <li><a href="#" data-filter=".audio">Audio</a></li>
                                <li><a href="#" data-filter=".video">Video</a></li>
                                <li><a href="#" data-filter=".documents">Documents</a></li>
                            </ul> --}}
                            <div class="btn-group pull-right">
                            <button v-on:click="changefilter(1)" class="btn btn-white btn-sm"><i class="fa fa-clock-o"></i> Past 24 HRS</button>
                                <button v-on:click="changefilter(7)" class="btn btn-white btn-sm"><i class="fa fa-calendar-o"></i> Past Week</button>
                                <button v-on:click="changefilter(14)" class="btn btn-white btn-sm"><i class="fa fa-calendar"></i> Past 2 Weeks</button>
                                <button v-on:click="changefilter(30)" class="btn btn-white btn-sm"><i class="fa fa-calendar"></i> Past Month</button>
                            </div>
                            {{-- <a href="#" type="button" class="btn pull-right btn-sm"><i class="fa fa-upload"></i> Upload New File</a> --}}



                            <div id="gallery" class="media-gal">
                                <div v-for="post in laravelData.data" :key="post.id" class="images item" >
                                <a v-bind:href="'#occasiondesc'+post.id" data-toggle="modal">
                                    <img v-bind:src="'images/'+post.imagepath" alt="" />

                                    <p>{% post.imagename %}</p> </a>
                                </div>
                                <div v-if="!laravelData.data.length" class="text-center">
                                    <h2>No Data Found!</h2>
                                </div>

                            </div>

                            <div class="col-md-12 text-center clearfix">
                                {{-- <ul class="pagination">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">»</a></li>
                                </ul> --}}
                                {{-- {{$occasions->links()}} --}}
                                <pagination :data="laravelData" @pagination-change-page="getResults"></pagination>
                            </div>
                                                        <!-- Modal -->
                                                        <div v-for="post in laravelData.data" :key="post.id" class="modal fade" v-bind:id="'occasiondesc'+post.id" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title">Incident Details</h4>
                                                                    </div>

                                                                    <div class="modal-body row">

                                                                        <div class="col-md-5 img-modal">
                                                                            <img v-bind:src="'images/'+post.imagepath" alt="">
                                                                            {{-- <a href="#" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit Image</a> --}}
                                                                            <div class="col-lg-6" style="padding-left: 0px;">
                                                                                <a v-bind:href="'/timeline/'+post.id" class="btn btn-white btn-sm"><i class="fa fa-info"></i> View TimeLine</a>
                                                                            </div>
                                                                            <div class="col-lg-6" style="padding-left: 0px;">
                                                                                <a v-bind:href="'images/'+post.imagepath" v-bind:data-lightbox="'image-'+post.id" v-bind:data-title="post.imagename" class="btn btn-white btn-sm"><i class="fa fa-eye"></i> View Full Size</a>

                                                                            </div>


                                                                            <p class="mtop10"><strong>Occasion Name:</strong> {% post.imagename %}</p>
                                                                            <p><strong>Capture From:</strong> {% post.capturefrom %}</p>
                                                                        </div>
                                                                        <div class="col-md-7">
                                                                            <form>
                                                                            @csrf
                                                                            <input type="hidden" v-bind:value="post.id" name="occasionid" />
                                                                            {{-- <div class="form-group">
                                                                                <label> Name</label>
                                                                                <input id="name" value="img01.jpg" class="form-control">
                                                                            </div> --}}
                                                                            <div class="form-group">
                                                                            <select name="userid" class="form-control" v-model="selecteduser">
                                                                                <option disabled value="">Please select one</option>
                                                                                <option v-for="e in employees" v-bind:value="e.id">{% e.name %}</option>
                                                                              </select>
                                                                            </div>
                                                                            {{-- <div class="form-group">
                                                                                <label> Tittle Text</label>
                                                                                <input id="title" value="awesome image" class="form-control">
                                                                            </div> --}}
                                                                            <div class="form-group">
                                                                                <label> Description</label>
                                                                            <textarea name="occasiondesc" rows="2" class="form-control">{% post.description %}</textarea>
                                                                            </div>
                                                                            {{-- <div class="form-group">
                                                                                <label> Link URL</label>
                                                                                <input id="link" value="images/gallery/img01.jpg" class="form-control">
                                                                            </div> --}}
                                                                            <div class="pull-right">
                                                                                <button class="clearfields btn btn-danger" type="reset">Clear</button>
                                                                                <button class="updateoccasion btn btn-primary" type="button">Save changes</button>
                                                                            </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
            </section>
        </section>
        <!--main content end-->
@endsection
@section('pagescripts')
    <script src="{{asset('vendor/lightbox/js/lightbox.js')}}"></script>
    <script src="js/notify.min.js"></script>
    <script src="js/app.js"></script>
    <script>

$(document).on('click','.updateoccasion',function() {
            var $form = $(this).parent().parent();
            var data = getFormData($form);
            console.log(data)
            var request = $.ajax({
                url: "/updateoccasion",
                type: "POST",
                data: data,
            });

            request.done(function(msg) {
                $.notify("Data Updated!", 'info');
            });

            request.fail(function(jqXHR, textStatus) {
                $.notify("Error while Saving!", 'error');
            });
        });


        function getFormData($form) {
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};
            $.map(unindexed_array, function(n, i) {
                indexed_array[n['name']] = n['value'];
            });

            return indexed_array;
        }


        </script>
@endsection
