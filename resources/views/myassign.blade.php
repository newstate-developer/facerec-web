@extends('layout.baselayout')

@section('title','My Assign')
@section('pagecss')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">


@stop
@section('content')



<!--main content start-->
<section id="main-content">
    <section class="wrapper">
    <!-- page start-->
<div class="row">
            <div class="col-lg-12">
               <h2>My Assign</h2>
            <table class="table table-bordered" id="laravel_datatable">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Status</th>
                     <th>Action</th>
                     <th>Currently Assign To</th>
                     <th>Created On</th>

                  </tr>
               </thead>
               <tbody>
                   @foreach ($myassign as $item)
                   <tr>
                    <td>
                        {{$item->image->imagename}}
                    </td>
                    <td>

                        @if($item->image->status == 0)
                        Pending
                        @elseif($item->image->status == 1)
                        Active
                        @elseif($item->image->status == 2)
                        Completed

                        @endif
                    </td>

                    <form method="post" action="{{url('assignroles')}}">
                        {{csrf_field()}}
                    <input type="hidden" name="image_id" value="{{$item->image->id}}">

                    <td>
                        @if($item->image->status == 0)
                        <input type="submit" class="btn btn-primary"name="action" value="Active">
                        @elseif($item->image->status == 1)
                        <input type="submit" class="btn btn-primary"name="action" value="Complete">
                        @elseif($item->image->status == 2)
                        <input type="button" disabled class="btn btn-primary"name="action" value="completed">

                        @endif
                        {{-- <input type="button" class="btn btn-primary"  onclick="window.location='{{ url("timeline") }}'" name="action" value="Time Line"> --}}
                    <a href="/timeline/{{$item->image->id}}" class="btn btn-primary">Open Timeline </a>
                    </td>
                    <td>
                        {{$item->User->name}}
                    </td>
                    <td>
                        {{$item->image->created_at}}
                    </td>
                </tr>
                   @endforeach
                </form>
               </tbody>
            </table>
         </div>
         </div>
         </section>
         </section>


        @stop

@section('pagescripts')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
   $(document).ready( function () {
    $('#laravel_datatable').DataTable( {
        "order": [[ 3, "asc" ]]
    } )
     });
  </script>
@stop
