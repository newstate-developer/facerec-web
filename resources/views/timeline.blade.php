@extends('layout.baselayout')

@section('title','Time Line')
@section('pagecss')
<style>


    /* Button used to open the chat form - fixed at the bottom of the page */
    .open-button {
      background-color: #555;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      opacity: 0.8;
      position: fixed;
      bottom: 23px;
      right: 28px;
      width: 280px;
    }

    /* The popup chat - hidden by default */
    .chat-popup {
      display: none;
      position: fixed;
      bottom: 0;
      right: 15px;
      border: 3px solid #f1f1f1;
      z-index: 9;
    }

    /* Add styles to the form container */
    .form-container {
      max-width: 300px;
      padding: 10px;
      background-color: white;
    }

    /* Full-width textarea */
    .form-container textarea {
      width: 100%;
      padding: 15px;
      margin: 5px 0 22px 0;
      border: none;
      background: #f1f1f1;
      resize: none;
      min-height: 200px;
    }

    /* When the textarea gets focus, do something */
    .form-container textarea:focus {
      background-color: #ddd;
      outline: none;
    }

    /* Set a style for the submit/send button */
    .form-container .btn {
      background-color: #4CAF50;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      margin-bottom:10px;
      opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
      background-color: red;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover, .open-button:hover {
      opacity: 1;
    }
    </style>
@stop
@section('content')
<!--main content start-->


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">

    <title>Timeline page</title>

    <!--Core CSS -->
    <link href="bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<!-- sidebar menu end-->

<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <div class="timeline">
                    <article class="timeline-item alt">
                         @if($results->first())
                        <div class="text-right">
                            <div class="time-show first">
                            <a href="#" class="btn btn-primary"> Timeline</a>
                            </div>
                        </div>
                        @else
                        <div class="text-center">
                            <div class="time-show first">
                            <a href="#" class="btn btn-primary">Not Assigned</a>
                            </div>
                        </div>
                        @endif
                    </article>

                    @if($results->first())
                    @foreach ($results as $item)
                    @if(!$item->content)
                    @if ( $item->still_assigned == 1)
                    <article class="timeline-item {{$item['user']['id'] == Auth::id() ? "alt" : ""}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                <h1 class="red">{{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</h1>
                                    <p>Incident was assigned to {{$item['user']['name']}} </p>
                                    <div class="album">
                                        <a>
                                        <img height="150px" alt="" src="/images/{{$item['image']['imagepath']}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    @elseif($item->still_assigned == 0)
                    <article class="timeline-item {{$item['user']['id'] == Auth::id()  ? "alt" : ""}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-power-off"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                <h1 class="red">{{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</h1>
                                    <p>Incident was taken from {{$item['user']['name']}} </p>
                                    <div class="album">
                                        <a>
                                        <img height="150px" alt="" src="/images/{{$item['image']['imagepath']}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endif
                    @endif

                    @if($item->still_assigned == null && $item->content)
                    <article class="timeline-item {{$item['from_user'] == Auth::id() ? "" : "alt"}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                <h1 class="red">{{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</h1>
                                    <p>{{$item['content']}} </p>
                                    <div class="album">
                                        <a>
                                        <img height="150px" alt="" src="/images/{{$item['image']['imagepath']}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endif

                    @if(array_key_exists('still_assigned',$item))
                    {{-- <article class="timeline-item {{$item['user']['id'] == Auth::id() ? "alt" : ""}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                <h1 class="red">{{$item['created_at']}}</h1>
                                    <p>Incident was assigned to {{$item['user']['name']}} </p>
                                    <div class="album">
                                        <a>
                                        <img height="150px" alt="" src="/images/{{$item['image']['imagepath']}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article> --}}
                    {{-- <article class="timeline-item {{$item->user->id == Auth::id() ? "alt" : ""}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                    <h1 class="red">{{$item->updated_at->diffForHumans()}}</h1>
                                    <p>Incident has taken from {{$item->user->name}} </p>
                                </div>
                            </div>
                        </div>
                    </article> --}}
                    {{-- @else
                    <article class="timeline-item {{$item->user->id == Auth::id() ? "alt" : ""}}">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <span class="arrow-alt"></span>

                                    <span class="timeline-icon red">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <span class="timeline-date" date="current_time">08:25 am</span>
                                    <h1 class="red">{{$item->updated_at->diffForHumans()}}</h1>
                                    <p>Incident was assigned to {{$item->user->name}} </p>
                                    <div class="album">
                                        <a>
                                        <img height="150px" alt="" src="/images/{{$item->image->imagepath}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article> --}}
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
    <!--main content end-->


<!-- Placed js at the end of the document so the pages load faster -->

<button class="open-button" onclick="openForm()">Chat</button>

<div class="chat-popup" id="myForm">
  <form method="POST" action="/send" class="form-container">
    @csrf
  <input type="hidden" name="image_id" value="{{$imgid}}">
    <h1>Chat</h1>

    <label for="msg"><b>Message</b></label>
    <textarea placeholder="Type message.." name="msg" required></textarea>

    <button type="submit" class="btn">Send</button>
    <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
  </form>
</div>
<script>
    function openForm() {
      document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
      document.getElementById("myForm").style.display = "none";
    }
    </script>

        @stop

@section('pagescripts')

@stop
