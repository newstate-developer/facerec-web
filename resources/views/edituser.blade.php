@extends('layout.baselayout')

@section('title','Edit User')
@section('pagecss')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}

@stop
@section('content')



<!--main content start-->
<section id="main-content">
    <section class="wrapper">
    <!-- page start-->
<div class="row">
            <div class="col-lg-12">
               <h2>Edit User</h2>
            <table class="table table-bordered" id="laravel_datatable">
               <thead>
                  <tr>
                     <th>Name</th>
                     {{-- <th>Status</th> --}}
                     <th>Role</th>

                  </tr>
               </thead>
               <tbody>
                @foreach ($users as $item)

                   <tr>
                    <td>
                     {{$item->name}}
                    </td>

                    <form method="post" action="/editrole" class="form-inline">
                       @csrf
                    <input type="hidden" name="user_id" value="{{$item->id}}">


                    <td>
                       <div class="input-group">

                       <div class="form-group">

                            {{-- <label for="exampleInputSelectRole">Select Role</label> --}}
                            <select name="role" class="form-control" style="width: 120px;">
                                  @if(Auth::user()->hasRole('super-admin'))
                                  <option {{$item->hasRole('manager') ? "selected" : ""}} value="manager">Manager</option>
                                 <option {{$item->hasRole('employee') ? "selected" : ""}}  value="employee">Employee</option>

                            </div>
                            <div class="form-control">
                                 <input type="submit" class="btn btn-primary"name="action" value="Save">
                                </div>
                               @endif


                       </div>
                            </td>
                </form>
                </tr>
                @endforeach

               </tbody>
            </table>
         </div>
         </div>
         </section>
         </section>


        @stop

@section('pagescripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
   $(document).ready( function () {
    $('#laravel_datatable').DataTable();
     });
  </script>
@stop
